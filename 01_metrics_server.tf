resource "helm_release" "metrics" {
  count     = var.metrics_server.enable == true ? 1 : 0
  namespace = kubernetes_namespace.monitoring.metadata[0].name

  name       = "metrics"
  repository = "https://kubernetes-sigs.github.io/metrics-server"
  chart      = "metrics-server"
  version    = var.metrics_server.version

  values = [yamlencode({
    "apiService" = {
      "insecureSkipTLSVerify" = "true"
      "create"                = "true"
    }

    "args" = [
      "--kubelet-insecure-tls",
    ]

    "resources" = {
      "requests" = {
        "cpu"    = "40m"
        "memory" = "100Mi"
      }
      "limits" = {
        "cpu"    = "800m"
        "memory" = "1Gi"
      }
    }
  })]

  depends_on = [kubernetes_namespace.monitoring]
}
