resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}

resource "kubernetes_namespace" "metallb" {
  count = var.metallb.enable == true ? 1 : 0
  metadata {
    labels = {
      "pod-security.kubernetes.io/enforce" = "privileged"
      "pod-security.kubernetes.io/audit"   = "privileged"
      "pod-security.kubernetes.io/warn"    = "privileged"
    }
    name = "metallb-system"
  }
}

resource "kubernetes_namespace" "longhorn" {
  count = var.longhorn.enable == true ? 1 : 0
  metadata {
    name = "longhorn-system"
  }
}

resource "kubernetes_namespace" "vault" {
  count = var.vault.enable == true ? 1 : 0
  metadata {
    name = "vault"
  }
}

resource "kubernetes_namespace" "consul" {
  count = var.consul.enable == true ? 1 : 0
  metadata {
    name = "consul"
  }
}

resource "kubernetes_namespace" "chaosmesh" {
  count = var.chaosmesh.enable == true ? 1 : 0
  metadata {
    name = "chaosmesh"
  }
}