# Install the longhorn helm chart
resource "helm_release" "longhorn" {
  count     = var.longhorn.enable == true ? 1 : 0
  namespace = kubernetes_namespace.longhorn[0].metadata[0].name

  name       = "longhorn"
  repository = "https://charts.longhorn.io"
  chart      = "longhorn"
  version    = var.longhorn.version

  depends_on = [kubernetes_namespace.longhorn]

  values = [
    yamlencode({
      "autoDeletePodWhenVolumeDetachedUnexpectedly" = true
      "disableSchedulingOnCordonedNode"             = false
      "replicaZoneSoftAntiAffinity"                 = false
      "replicaDiskSoftAntiAffinity"                 = false
    })
  ]

  timeout = 3000
}