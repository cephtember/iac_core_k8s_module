resource "helm_release" "vault" {
  count     = var.vault.enable == true ? 1 : 0
  namespace = kubernetes_namespace.vault[0].metadata[0].name

  name       = "vault"
  repository = "https://helm.releases.hashicorp.com"
  chart      = "vault"
  version    = var.vault.version

  values = [
    yamlencode({
      "serverTelemetry" = {
        "serviceMonitor" = {
          "enabled" = false
        }
      }
      "serviceAccount" = {
        "create" = true
        "name"   = "vault"
      }


      "standalone" = {
        "enabled" = true
      }

      "ui" = {
        "enabled" = true
        "serviceType" : "ClusterIP"
        "externalPort" = 8200
        "targetPort"   = 8200
      }
    })
  ]

  depends_on = [
    helm_release.ingress-nginx,
  ]

  lifecycle {
    prevent_destroy = true
  }

}

resource "kubernetes_ingress_v1" "vault" {
  count = var.vault.ingress.enable == true ? 1 : 0
  metadata {
    name      = "vault-ingress"
    namespace = kubernetes_namespace.vault[0].metadata[0].name
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
    }
  }
  spec {
    rule {
      host = var.vault.ingress.hostname
      http {
        path {
          path = "/"
          backend {
            service {
              name = "vault-ui"
              port {
                number = 8200
              }
            }
          }
        }
      }
    }
  }

  depends_on = [helm_release.ingress-nginx]
}