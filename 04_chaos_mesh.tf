resource "helm_release" "chaosmesh" {
  count     = var.chaosmesh.enable == true ? 1 : 0
  namespace = kubernetes_namespace.chaosmesh[0].metadata[0].name

  name       = "chaos-mesh"
  repository = "https://charts.chaos-mesh.org"
  chart      = "chaos-mesh"
  version    = var.chaosmesh.version

  values = [
    yamlencode({
      "chaosDaemon" = {
        "runtime"    = "containerd"
        "socketPath" = "/run/containerd/containerd.sock"
      }
  })]

  depends_on = [kubernetes_namespace.chaosmesh]
}