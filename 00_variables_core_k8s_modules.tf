variable "kubeconfig_file" {
  type    = string
  default = "~/.kube/config"
}

variable "metallb" {
  type = object({
    enable     = bool
    version    = string
    ip_pool    = string
    autoassign = bool
  })
  default = {
    enable     = true
    version    = "latest"
    ip_pool    = "10.23.31.220-10.23.31.230"
    autoassign = true
  }
}

variable "longhorn" {
  type = object({
    enable  = bool
    version = string
  })
  default = {
    enable  = true
    version = "latest"
  }
}

variable "ingress_nginx" {
  type = object({
    enable = bool
    modsecurity = object({
      enable = bool
    })
    owasp = object({
      enable = bool
    })
    opentracing = object({
      enable                = bool
      jaeger_collector_host = string
    })
    version = string
  })
  default = {
    enable = true
    modsecurity = {
      enable = false
    }
    owasp = {
      enable = false
    }
    opentracing = {
      enable                = false
      jaeger_collector_host = "jaeger-agent.monitoring.svc.cluster.local"
    }
    version = "latest"
  }
}

variable "metrics_server" {
  type = object({
    enable  = bool
    version = string
  })
  default = {
    enable  = true
    version = "latest"
  }
}

variable "vault" {
  type = object({
    ingress = object({
      enable   = bool
      hostname = string
    })
    enable  = bool
    version = string
  })
  default = {
    enable  = true
    version = "latest"
    ingress = {
      enable   = true
      hostname = "vault.k8s.loc"
    }
  }
}

variable "consul" {
  type = object({
    enable  = bool
    version = string
  })
  default = {
    enable  = false
    version = "latest"
  }
}

variable "external_dns" {
  type = object({
    enable  = bool
    version = string
    rfc2136 = object({
      host        = string
      port        = string
      tsigSecret  = string
      tsigKeyname = string
      zone        = string
    })
  })
  default = {
    enable  = true
    version = "latest"
    rfc2136 = {
      host        = "10.23.31.69"
      port        = "53"
      tsigSecret  = "SHgLMsmVE+HsapBekveY3NBsbE8PmLA01MSJti/OjA8="
      tsigKeyname = "rndc-key"
      zone        = "k8s.loc"
    }
  }
}

variable "chaosmesh" {
  type = object({
    enable  = bool
    version = string
  })
  default = {
    enable  = false
    version = "latest"
  }
}
