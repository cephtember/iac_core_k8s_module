# Install the metallb helm chart
resource "helm_release" "metallb" {
  count     = var.metallb.enable == true ? 1 : 0
  namespace = kubernetes_namespace.metallb[0].metadata[0].name

  name       = "metallb"
  repository = "https://metallb.github.io/metallb"
  chart      = "metallb"
  version    = var.metallb.version

  depends_on = [kubernetes_namespace.metallb]

  values = [
    yamlencode({
      "speaker" = {
        "frr" = {
          "enabled" = false
        }
      }
    })
  ]
}

resource "kubectl_manifest" "metallb_address_pool" {
  yaml_body = <<YAML
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: default
  namespace: metallb-system
spec:
  addresses:
  - ${var.metallb.ip_pool}
  autoAssign: ${var.metallb.autoassign}
YAML

  depends_on = [
    helm_release.metallb,
  ]
}

resource "kubectl_manifest" "metallb_l2_adv" {
  yaml_body = <<YAML
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: default
  namespace: metallb-system
spec:
  ipAddressPools:
  - default
YAML

  depends_on = [
    kubectl_manifest.metallb_address_pool,
  ]
}
