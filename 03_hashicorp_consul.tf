resource "helm_release" "consul" {
  count     = var.consul.enable == true ? 1 : 0
  namespace = kubernetes_namespace.consul[0].metadata[0].name

  name       = "consul"
  repository = "https://helm.releases.hashicorp.com"
  chart      = "consul"
  version    = var.consul.version

  values = [
    yamlencode({
      "global" = {
        "name" = "consul"
      }
    })
  ]
  depends_on = [kubernetes_namespace.consul]
}