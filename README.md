# Модуль terraform освновных компонентов k8s

Модуль используется в качестве личных экспериментов и сделан просто ради фана.

> На самом деле я просто устал пересобирать
> себе кластер для тестирования каких либо
> сценариев / чартов / приложений.

## TODO
- Добавить установку prometheus stack
- Добавить установку Opensearch + logstash + fluentbit
- Добавить установку keel
- Добавить установку jaeger-tracing
- Добавить установку chaos-mesh


## Features

- Установка longhorn ebs storage
- Установка metallb load balancer
- Установка metrics server
- Установка external-dns updater
- Установка ingress nginx
- Установка consul
- Установка vault


## Installation

  Устанавливаем необходимые приложения и зависимости
  
  Macbook M1 Fix:
  ```
  brew uninstall terraform
  brew install tfenv
  TFENV_ARCH=amd64 tfenv install 1.4.6
  tfenv use 1.4.6
  ```

  Для работы metallb необходимо переключить strictAPR в значение true

```
kubectl get configmap kube-proxy -n kube-system -o yaml | \
sed -e "s/strictARP: false/strictARP: true/" | \
kubectl diff -f - -n kube-system

kubectl get configmap kube-proxy -n kube-system -o yaml | \
sed -e "s/strictARP: false/strictARP: true/" | \
kubectl apply -f - -n kube-system
```

  Приводим terraform main.tf фаил к следующему виду

  ```module "core_k8s_modules" {
#  Путь до модуля (может быть как git repo или путь до папки)
#  
  source = "git@gitlab.com:cephtember/iac_core_k8s_tf_module.git"

#  Путь до kubeconfig файла
#  
  kubeconfig_file = "k8s_local_kube_config"


#  Настройки metallb (Optional)
#
#  metallb = {
#    enable = true #[default: true]
#    version = "0.13.12" #[default: latest]
#    ip_pool = "10.23.31.220-10.23.31.230" #[default: 192.168.0.30-192.168.0.40]
#    autoassign = true #[default: true]
#  }        



#  Настройки longhorn (Optional)
#
#  Important: По умолчанию в модуле запрещено хранить реплику в той же зоне
#  Необходимо выставить label на ноде topology.kubernetes.io/zone=<Zone name of the node>
#
#  longhorn = {
#    enable = true #[default: true]
#    version = "1.5.3" #[default: latest]
#    replicaZoneSoftAntiAffinity = false
#    replicaDiskSoftAntiAffinity = false
#  }


#  Настройки ingress nginx (Optional)
#
#  ingress_nginx = { 
#    enable = true #[default: true]
#    version = "4.9.0" #[default: latest]
#    
#    modsecurity = {
#      enable = false #[default: false]
#    }
#    owasp = {
#      enable = false #[default: false]
#    }
#    opentracing = {
#      enable = false #[default: false]
#      jaeger_collector_host = "jaeger-agent.monitoring.svc.cluster.local"
#    }
#  }

#  Настройки metricsserver (Optional)
#
#  metrics_server = {
#    enable = true #[default: true]
#    version = "3.11.0" #[default: latest]
#  }

#  Настройки vault
#
#  vault = {
#    enable = true #[default: ]
#    version = "0.27.0" #[default: latest]
#    ingress = {
#      enable = true #[default: true]
#      hostname = "vault.k8s.loc" #[default: vault.k8s.loc]
#    }
#  }


#  Настройки consul (Optional)
#
#  consul = {
#    enable = false #[default: false]
#    version = "latest" #[default: latest]
#  }


# Настройки external dns для обновления записей (Optional)
#
#  external_dns = {
#    enable = true #[default: true]
#    version = "6.28.6" #[default: latest]
#
#    rfc2136 = {
#      host = "10.23.31.69" #[default: 10.23.31.69]
#      port = "53" #[default: 53]
#      tsigSecret = "SHgLMsmVE+HsapBekveY3NBsbE8PmLA01MSJti/OjA8=" #[default: SHgLMsmVE+Hsa...]
#      tsigKeyname = "rndc-key" #[default: rndc-key]
#      zone = "k8s.loc" #[default: k8s.loc]
#    }
#  }
#}


#  Настройки chaosmesh (Optional)
#
#  chaosmesh = {
#    enable = false #[default: false]
#    version = "latest" #[default: latest]
#  }
```

## Plugins

| Plugin | README |
| ------ | ------ |
| 00_Main Module | [https://gitlab.com/cephtember/iac_tf_project] |
| 01_CloudInit Module | [https://gitlab.com/cephtember/iac_cloudinit_tf_module] |
| 02_Core Module | [https://gitlab.com/cephtember/iac_core_k8s_tf_module] |
| 03_Security Module | [https://gitlab.com/cephtember/iac_security_k8s_tf_module] |