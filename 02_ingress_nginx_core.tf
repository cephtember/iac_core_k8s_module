resource "helm_release" "ingress-nginx" {
  count     = var.ingress_nginx.enable == true ? 1 : 0
  namespace = "default"

  name       = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = var.ingress_nginx.version

  values = [
    yamlencode({
      "controller" = {
        "kind"      = "DaemonSet"
        "extraArgs" = {}
        "prometheus" = {
          "create" = true
          "port"   = 10254
        }
        "metrics" = {
          "enabled" = "true"
          "service" = {
            "annotations" = {
              "prometheus.io/port"   = "10254"
              "prometheus.io/scrape" = "true"
              "prometheus.io/scheme" = "http"
            }
          }
        }
        "podAnnotations" = {
          "prometheus.io/port"   = "10254"
          "prometheus.io/scrape" = "true"
        }
        "config" = {
          "enable-opentracing"           = var.ingress_nginx.opentracing.enable == true ? "true" : "false"
          "jaeger-collector-host"        = var.ingress_nginx.opentracing.enable == true ? var.ingress_nginx.opentracing.jaeger_collector_host : ""
          "use-gzip"                     = "true"
          "proxy-body-size"              = "100m"
          "proxy-connect-timeout"        = 300
          "proxy-read-timeout"           = 300
          "proxy-send-timeout"           = 300
          "log-format-escape-json"       = true
          "log-format-upstream"          = "{\"time\": \"$time_iso8601\", \"remote_addr\": \"$remote_addr\", \"status\": $status, \"host\": \"$host\", \"path\": \"$uri\", \"request_query\": \"$args\", \"method\": \"$request_method\", \"proxy_upstream_name\": \"$proxy_upstream_name\", \"ingress_name\": \"$ingress_name\"}"
          "enable-modsecurity"           = var.ingress_nginx.modsecurity.enable == true ? "true" : "false"
          "enable-owasp-modsecurity-crs" = var.ingress_nginx.owasp.enable == true ? "true" : "false"
          "compute-full-forwarded-for"   = "true"
          "use-forwarded-headers"        = "false"
          "real-ip-header"               = "proxy_protocol"

          "modsecurity-snippet" = <<-EOT
            # Enable prevention mode. Can be any of: DetectionOnly,On,Off (default is DetectionOnly)
            SecRuleEngine On
            # Enable scanning of the request body
            SecRequestBodyAccess On
            # Enable XML and JSON parsing
            SecRule REQUEST_HEADERS:Content-Type "(?:application(?:/soap\+|/)|text/)xml" "id:200000,phase:1,t:none,t:lowercase,pass,nolog,ctl:requestBodyProcessor=XML"
            SecRule REQUEST_HEADERS:Content-Type "application/json" "id:200001,phase:1,t:none,t:lowercase,pass,nolog,ctl:requestBodyProcessor=JSON"
            SecAction "id:900200,phase:1,nolog,pass,t:none,setvar:tx.allowed_methods=GET HEAD POST OPTIONS PUT PATCH DELETE"
            # https://github.com/SpiderLabs/owasp-modsecurity-crs/issues/1566
            SecRuleRemoveById 920350
            # Send ModSecurity audit logs to the stdout (only for rejected requests)
            SecAuditLog /dev/stdout
            SecAuditLogFormat JSON
            SecAuditEngine RelevantOnly # could be On/Off/RelevantOnly
          EOT
        }
        "service" = {
          "externalTrafficPolicy" = "Local"
        }
        "tolerations" = [
          {
            "effect"   = "NoSchedule"
            "operator" = "Exists"
          },
        ]
      },

    })
  ]
  depends_on = [helm_release.metallb]

  timeout = 3000
}
