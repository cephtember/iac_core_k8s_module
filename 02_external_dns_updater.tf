resource "helm_release" "external_dns" {
  count     = var.external_dns.enable == true ? 1 : 0
  namespace = "default"

  name       = "external-dns"
  repository = "oci://registry-1.docker.io/bitnamicharts"
  chart      = "external-dns"
  version    = var.external_dns.version


  values = [
    yamlencode({
      "provider" = "rfc2136"
      "rfc2136" = {
        "host"        = var.external_dns.rfc2136.host
        "port"        = var.external_dns.rfc2136.port
        "tsigSecret"  = var.external_dns.rfc2136.tsigSecret
        "tsigKeyname" = var.external_dns.rfc2136.tsigKeyname
        "zone"        = var.external_dns.rfc2136.zone
      }
    })
  ]
}